"""
VCS Prüfungsstatistiken - anonymisation and typesetting of exam result statistics
Copyright (C) 2022  Henry Wetton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from reportlab.platypus import TableStyle
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import landscape, A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.enums import TA_LEFT, TA_RIGHT
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import pandas as pd
import os
from datetime import date
path = os.path.dirname(__file__)

# General style
styles = getSampleStyleSheet()

# Font
pdfmetrics.registerFont(TTFont('Source Sans Pro', 
                               'fonts/SourceSansPro-Regular.ttf'))
pdfmetrics.registerFont(TTFont('Source Sans Pro Light', 
                               'fonts/SourceSansPro-Light.ttf'))


# Study programme codes
codes = pd.read_csv(f'{path}/codes.csv', encoding='utf-8')

# Data columns
cols = {'reglement': 'Reglement',
        'pruefungsstufe': 'Prüfungsstufe',
        'lk_nummer': 'LE-Nummer',
        'le_titel': 'Block/Fach',
        'anzahl_stud': 'Anzahl\nStud.',
        'durchschnitt': 'Durch-\nschnitt',
        'standardabweich': 'Std.-\nabw.',
        'anzahl_unterbr': 'Anzahl\nUnterbr.',
        'anzahl_best': 'Anzahl\nbest.',
        'anzahl_n_best': 'Anzahl\nn. best.',
        'davon_abbrueche': 'Davon\nAbbr.',
        'prozent_best': '% best.',
        'anzahl_rept': 'Anzahl\nRepet.',
        'anzahl_unterbr.1': 'Anzahl\nUnterbr.',
        'anzahl_best.1': 'Anzahl\nbest.',
        'anzahl_n_best.1': 'Anzahl\nn. best.',
        'davon_abbrueche_rept': 'Davon\nAbbr.',
        'prozent_best.1': '% best.',
        'anzahl_stud.1': 'Anzahl\nStud.',
        'durchschnitt.1': 'Durch-\nschnitt',
        'standardabweich.1': 'Std.-\nabw.',
        'prozent_best.2': '% best.'
}    


# Page setup
PAGE_SIZE = landscape(A4)
page_w = PAGE_SIZE[0]
page_h = PAGE_SIZE[1]

logo_vcs_w = 4.67*cm
logo_vcs_h = 1.5*cm
logo_vseth_w = 7.43*cm
logo_vseth_h = 1.39*cm
margin_x = 0.4*cm
margin_y = 0.4*cm
line_height = 0.4*cm

MARGINS = {
    'topMargin': margin_y + 6.5*line_height,
    'bottomMargin': line_height + margin_y,
    'leftMargin': margin_x,
    'rightMargin': margin_x,
}

def draw_page(canvas, doc):
    canvas.saveState()

    # Extract title data
    (regl_code, lvl, semester) = doc.title.split(':')
    prog = list(codes.loc[codes['code'] == regl_code, 'prog'])[0]
    regl_date = list(codes.loc[codes['code'] == regl_code, 'date'])[0]

    # Header
    margin_extra_logos = 0.2*cm
    canvas.drawImage('logo_vcs.png', 
                     x=(margin_x + margin_extra_logos), 
                     y=(page_h - logo_vcs_h - margin_y - margin_extra_logos), 
                     width=logo_vcs_w, height=logo_vcs_h)
    canvas.drawImage('logo_vseth.png', 
                     x=(page_w - logo_vseth_w - margin_x - margin_extra_logos), 
                     y=(page_h - logo_vseth_h - margin_y - margin_extra_logos), 
                     width=logo_vseth_w, height=logo_vseth_h)
    canvas.setFont('Source Sans Pro Light', 10)
    canvas.drawCentredString(page_w/2.0, page_h-margin_y-line_height, 
                             'Notendurchschnitte pro Fach und Stufe')
    canvas.drawCentredString(page_w/2.0, page_h-margin_y-2*line_height, 
                             f'Berücksichtigte Noten: {semester}, verfügte Noten')
    canvas.drawCentredString(page_w/2.0, page_h-margin_y-3.5*line_height, 
                             f'Studiengang: {prog}')
    canvas.drawCentredString(page_w/2.0, page_h-margin_y-4.5*line_height, 
                             f'Reglement: {regl_date}')
    canvas.drawCentredString(page_w/2.0, page_h-margin_y-5.5*line_height, 
                             f'Prüfungsstufe: {lvl}')

    # Footer
    date_str = date.today().strftime("%d.%m.%Y")
    canvas.drawCentredString(page_w/2.0, margin_y, 
                             f'Stand: {date_str}')

    canvas.restoreState()

BUILD_ARGS = {
    'onFirstPage': draw_page, 
    'onLaterPages': draw_page
}

# Styling for tables
lw_normal = 0.4
lw_thick = 1.2
lw_dbl = 1.2
GRID_STYLE = TableStyle([
    ('SPAN', (0,0), (1,1)),
    ('SPAN', (2,0), (9,0)),
    ('SPAN', (10,0), (15,0)),
    ('SPAN', (16,0), (-1,0)),
    ('LINEABOVE', (0,0), (-1,0), lw_normal, colors.gray),
    ('LINEBELOW', (0,0), (-1,0), lw_normal, colors.gray),
    ('LINEBELOW', (0,1), (-1,1), lw_thick, colors.gray),
    ('LINEBELOW', (0,2), (-1,-1), lw_normal, colors.gray),
    ('LINEAFTER', (2,0), (-2,-1), lw_normal, colors.gray),
    ('LINEAFTER', (1,0), (1,-1), lw_thick, colors.gray),
    ('LINEAFTER', (9,0), (9,-1), lw_dbl, colors.gray),
    ('LINEAFTER', (9,0), (9,-1), lw_dbl/3.0, colors.white),
    ('LINEAFTER', (15,0), (15,-1), lw_dbl, colors.gray),
    ('LINEAFTER', (15,0), (15,-1), lw_dbl/3.0, colors.white),
    ('ALIGN', (0,2), (1,-1), 'LEFT'),
    ('ALIGN', (2,0), (-1,0), 'CENTER'),
    ('ALIGN', (2,2), (-1,-1), 'RIGHT'),
    ('VALIGN', (0,0), (-1,2), 'BOTTOM'),
    ('VALIGN', (0,2), (-1,-1), 'MIDDLE'),
    ('FONT', (0,0), (-1,-1), 'Source Sans Pro'),
    ('FONTSIZE', (0,0), (-1,-1), 8),
    ('LEFTPADDING', (0,0), (-1,-1), 1),
    ('RIGHTPADDING', (0,0), (-1,-1), 1),
    ('BOTTOMPADDING', (0,0), (-1,-1), 2),
    ('TOPPADDING', (0,0), (-1,-1), 2)
])

table_text_style = styles['BodyText']
table_text_style.fontName = 'Source Sans Pro'
table_text_style.fontSize = 8
table_text_style.alignment = TA_LEFT

table_head_style = styles['Normal']
table_head_style.fontName = 'Source Sans Pro'
table_head_style.fontSize = 8
table_head_style.alignment = TA_RIGHT

# Column width/row height for tables
"""Columns of output:
    ['code', 'prog', 'date', 'reglement'] deleted 
    ['pruefungsstufe', 'lk_nummer',
       'le_titel', 'anzahl_stud', 'durchschnitt', 'standardabweich',
       'anzahl_unterbr', 'anzahl_best', 'anzahl_n_best', 'davon_abbrueche',
       'prozent_best', 'anzahl_rept', 'anzahl_unterbr.1', 'anzahl_best.1',
       'anzahl_n_best.1', 'davon_abbrueche_rept', 'prozent_best.1',
       'anzahl_stud.1', 'durchschnitt.1', 'standardabweich.1',
       'prozent_best.2']"""
cw = (2.0*cm, 6.6*cm,) + (1.1*cm,) * 18
rh = None

# Columns containing float data (averages/std. devs.) & percentages
float_cols = [3, 4, 17, 18]
pc_cols = [9, 15, 19]
