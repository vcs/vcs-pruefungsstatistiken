"""
VCS Prüfungsstatistiken - anonymisation and typesetting of exam result statistics
Copyright (C) 2022  Henry Wetton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import pandas as pd
import numpy as np
from reportlab.platypus import Paragraph, SimpleDocTemplate, tables
from PyPDF4 import PdfFileMerger
import os
PATH = os.path.dirname(__file__)
import pdf_setup


def read_data(filename: str, codes: pd.DataFrame) -> pd.DataFrame:
    """Read data from Excel file and add course data"""
    if 'C:' in filename:
        data_raw = pd.read_excel(filename)
    else:
        data_raw = pd.read_excel(os.path.join(PATH, filename))

    if not all(data_raw.columns == pd.Index(pdf_setup.cols.keys())):
        raise ValueError('Wrong input file format.'
                         'Ensure columns are named and ordered correctly.'
                        )

    data_full = pd.merge(codes, data_raw, how='right',
                         left_on='code', right_on='reglement'
                        )

    ffill_cols = ['anzahl_stud', 'anzahl_rept', 'anzahl_stud.1']
    data_full.loc[:, ffill_cols] = data_full.loc[:, ffill_cols].ffill()

    return data_full


def anonymise_row(row: pd.Series):
    """
    Anonymise a single row of data.

    For the data relating to:
    - the single degree course listed
    - students repeating the exam
    - all students taking the exam,

    remove all data where the number of students is 5 or fewer.
    """
    cols_list = list(pdf_setup.cols.keys())

    col_nr_single = cols_list[4]
    cols_single = cols_list[5:12]
    if row[col_nr_single] <= 5:
        row[cols_single] = np.nan

    col_nr_rep = cols_list[12]
    cols_rep = cols_list[13:18]
    if row[col_nr_rep] <= 5:
        row[cols_rep] = np.nan

    col_nr_all = cols_list[18]
    cols_all = cols_list[19:]
    if row[col_nr_all] <= 5:
        row[cols_all] = np.nan

    return row


def anonymise(data: pd.DataFrame):
    """
    Anonymise data.

    Remove any grade information concerning 5 or less students.
    For exam blocks, remove data for all contained subjects.
    """
    data_out = data.apply(axis=1, func=anonymise_row, result_type='broadcast')
    return data_out


def transform_data(data: pd.DataFrame) -> list:
    """Transform data into appropriate list for table"""
    data_list = data.iloc[:, 5:].to_numpy(na_value='').tolist()
    for row in data_list:
        row[0] = Paragraph(row[0], pdf_setup.table_text_style)
        row[1] = Paragraph(row[1], pdf_setup.table_text_style)
        for i in range(2, len(row)):
            if row[i] == '':  # NaN values
                continue
            if i in pdf_setup.float_cols:  # Float values
                row[i] = f'{row[i]:0.2f}'
            elif i in pdf_setup.pc_cols:  # Percentage values
                row[i] = f'{row[i]:0.1f}'
            else: # Integer values
                row[i] = f'{int(row[i]):d}'

    col_names_raw = data.columns[5:]
    col_names = []
    for i, col in enumerate(col_names_raw):
        if i < 2:
            col_names.append(Paragraph(pdf_setup.cols[col], pdf_setup.table_text_style))
        else:
            col_names.append(Paragraph(pdf_setup.cols[col], pdf_setup.table_head_style))

    upper_col_names = ['Block/Fach', '',
                       'aus gegebenem Studiengang', '', '', '', '', '', '', '',
                       'davon Repetenten', '', '', '', '', '', 
                       'alle Studiengänge (ohne Unterbr.)', '', '', '']
    
    return [upper_col_names, col_names, *data_list]


def make_pdf(data: pd.DataFrame, reglement: str, pruefungsstufe: str, semester: str, tmp_path: str = None):
    """Write data for a single subject to PDF file"""
    code_fn = reglement.replace(', ', '_').replace(' ', '_')
    lvl_fn = pruefungsstufe.replace(' ', '_')
    if tmp_path is None:
        tmp_path = os.path.join(PATH, "output", "tmp")
    filename = os.path.join(tmp_path, f'{code_fn}_{lvl_fn}.pdf')

    data_list = transform_data(data)

    doc = SimpleDocTemplate(filename, pagesize=pdf_setup.PAGE_SIZE,
                            **pdf_setup.MARGINS)
    doc.title = f'{reglement}:{pruefungsstufe}:{semester}'

    t = tables.LongTable(data_list, repeatRows=2,
                         colWidths=pdf_setup.cw, rowHeights=pdf_setup.rh, 
                         style=pdf_setup.GRID_STYLE)
    story = []
    story.append(t)
    
    try:
        doc.build(story, **pdf_setup.BUILD_ARGS)
    except IndexError:
        print(f'WARNING: degree programme "{reglement}" ({pruefungsstufe}) not found in codes.csv. Please add a corresponding entry if this should be included in the list of exam statistics.')


def clear_output():
    overwrite_yn = ''  # Replace '' with 'y' for testing
    while overwrite_yn not in ['y', 'n']:
        overwrite_yn = input(
            'Proceeding will overwrite all files in the output folder. '
            'Continue (y/n)? '
        ).lower()
    if overwrite_yn == 'y':
        with os.scandir(os.path.join(PATH, "output")) as out_dir:
            for f in out_dir:
                if not f.is_dir():
                    os.remove(f)
        return True
    else:
        print('Stopping execution...')
        return False


def combine_pdfs(tmp_path, out_path, stufen_extra):
    merger = PdfFileMerger(strict=True)

    stufen = ['Basisprüfung', 'Bachelor', 'Master Auflagen', 'Master']
    for stufe in stufen_extra:
        if stufe not in stufen:
            stufen.append(stufe)

    for i, r in pdf_setup.codes.iterrows():
        #
        reg = r['code'].replace(', ', '_').replace(' ', '_')
        for s in stufen:
            st = s.replace(' ', '_')
            fn = os.path.join(tmp_path, f'{reg}_{st}.pdf')
            if os.path.isfile(fn):
                bookmark_name = (f'{pdf_setup.codes.loc[i, "prog"]} '
                                 f'({pdf_setup.codes.loc[i, "date"]}) - {s}')
                merger.append(fileobj=fn, import_bookmarks=False, bookmark=bookmark_name)

    merger.write(fileobj=out_path)
    merger.close()


def main():
    filename_in = input('Enter name of file to import: ')
    filename_in_base = os.path.splitext(os.path.basename(filename_in))[0]
    semester_default = filename_in_base[-3:]
    semester = input(f'Enter semester code (default {semester_default}): ')
    if semester == '':
        semester = semester_default
    # filename_in = 'C:/Users/henry/VCS-owncloud/Statistiken/StatistikW22.xls'
    data = read_data(filename_in, pdf_setup.codes)

    data = anonymise(data)  # Comment out to test
    
    try:
        os.mkdir(os.path.join(PATH, 'output'))
    except OSError as error:
        print('Output directory already exists.')
        output_cleared = clear_output()
    else:
        print('Successfully created output directory.')
        output_cleared = True

    if output_cleared:
        output_dir = os.path.join(PATH, "output")
        tmp_dir = os.path.join(output_dir, "tmp")
        os.makedirs(tmp_dir, exist_ok=True)
        filename_out = os.path.join(output_dir, f'{filename_in_base}.pdf')

        data.to_csv(os.path.join(output_dir, 'Pruefungsstatistik_anon.csv'), index=False)
        
        for r in data['reglement'].unique():
            arr_r = (data['reglement'] == r)
            for s in data['pruefungsstufe'].unique():
                arr_s = (data['pruefungsstufe'] == s)
                arr_comb = (arr_r & arr_s)
                if any(arr_comb):
                    make_pdf(data.loc[arr_comb, :], r, s, semester=semester, 
                             tmp_path=tmp_dir)
            # break  # Remove '#' for testing

        combine_pdfs(tmp_dir, filename_out, data['pruefungsstufe'].unique())
        print(f'Result output to {filename_out}.')

        
	
		
if __name__ == '__main__':
	main()
