# VCS Prüfungsstatistiken

Python script to convert exam statistics (.xls) to a correctly formatted PDF file, redacted as necessary

## Prerequisites
- [Python](https://www.python.org/downloads/) (tested using Python 3.10.7)
- Python libraries (simply install via `pip install <library-name>`):
    - [NumPy](https://numpy.org/)
    - [Pandas](https://pandas.pydata.org/)
    - [ReportLab](https://www.reportlab.com)
    - [PyPDF4](https://github.com/claird/PyPDF4)

## Instructions
1. Save `.xls` file with statistics (obtained from Anina Frieden as of HS22) to local directory
2. Download this repo (including font files etc.!) & unpack to preferred location
3. Execute `exam_statistics.py` and enter path to `.xls` file when requested
	-> Output is generated in same parent folder as Python script under `output/<name-of-.xls-file>.pdf`
4. Check generated PDF file:
	1. Is the content consistent with the `.xls` file?
	2. Was the redacting done correctly? (Any statistic referring to a group of 5 students or less should have been removed.)
5. Publish!

## Further information on methods used
- [Reading Excel files into Pandas](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html)
- [ReportLab User Guide](https://www.reportlab.com/docs/reportlab-userguide.pdf)
